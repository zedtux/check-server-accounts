# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'check_server_accounts/version'

Gem::Specification.new do |spec|
  spec.name          = 'check-server-accounts'
  spec.version       = CheckServerAccounts::VERSION
  spec.authors       = ['Guillaume Hain']
  spec.email         = ['zedtux@zedroot.org']

  spec.summary       = 'Check if your account is working on all your servers.'
  spec.description   = 'Check if your account is working on all your servers.'
  spec.homepage      = 'https://github.com/zedtux/check-server-accounts'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_development_dependency 'bundler', '~> 1.10'
  spec.add_development_dependency 'rake', '~> 10.0'

  spec.add_dependency 'net-ssh', '~> 2.10.0.beta1'
  spec.add_dependency 'net-scp'
end
