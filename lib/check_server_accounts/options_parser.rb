require 'optparse'

module CheckServerAccounts
  #
  # Program options parser
  #
  module OptionsParser
    def self.parse!(arguments)
      options = { servers: [] }

      build_parser_and_parse(options, arguments)

      options
    end

    def self.build_parser_and_parse(options, arguments)
      @options_parser = build_parser(options)
      @options_parser.parse!(arguments)

      show_usage if options[:help]
      show_missing_server_error! if options[:servers].empty?

    rescue OptionParser::InvalidOption => error
      puts error
      puts @options_parser
      exit 1
    end

    def self.build_parser(options)
      OptionParser.new do |opts|
        opts.banner = "Usage: #{$PROGRAM_NAME} [options]"

        opts.on('-h', '--help', 'Show the help message') do
          options[:help] = true
        end

        opts.on('-i', '--install-ssh-key', 'When the SSH key is missing and ' \
                                           'the password allow connection ' \
                                           'install the SSH key') do
          options[:install_ssh_key] = true
        end

        opts.on('-p', '--password PASSWORD', 'Password to be used when ' \
                                             'running sudo command') do |val|
          options[:password] = val
        end

        opts.on('-s', '--server SERVER_NAME', 'Server to be checked') do |val|
          options[:servers] << val
        end

        opts.on('-u', '--username USERNAME', 'Username to be used when ' \
                                             'connecting to a server') do |val|
          options[:username] = val
        end
      end
    end

    def self.show_usage(exit_code = 0)
      puts @options_parser
      exit exit_code
    end

    def self.show_missing_server_error!
      puts 'ERROR: You must provide at least one server.'
      puts
      show_usage(1)
    end
  end
end
