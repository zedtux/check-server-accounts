module CheckServerAccounts
  #
  # Program actions on demand like SSH key installation
  #
  module Action
    def self.take(action, ssh_client)
      case action
      when :install_ssh_key
        ssh_client.mkdir_p('.ssh/')
        ssh_client.cat('~/.ssh/id_rsa.pub', to: '.ssh/authorized_keys')
      end
    end
  end
end
