require 'net/ssh'
require 'net/scp'

module CheckServerAccounts
  #
  # This class manage the SSH object for a given host, username and password
  #
  class SshClient
    class NotConnectedError < StandardError; end

    def initialize(hostname, username, password, state_machine)
      @hostname = hostname
      @username = username
      @password = password
      @state_machine = state_machine
      @output = ''
    end

    #
    # Try first to connect first with publickey and then by password (in the
    # case the publickey failed).
    # Then execute the sudo check
    #
    def connect
      ssh_connect
      execute_ssh("sudo -l -U #{@username}") if ssh_connected?
      true
    end

    def connected?
      @ssh
    end

    def output
      @output
    end

    def cat(path, options = {})
      remote_path = File.join('/tmp', 'csa')
      execute_ssh("mkdir -p #{remote_path}")

      remote_filepath = File.join(remote_path, Time.now.to_i.to_s)
      begin
        @ssh.scp.upload(path.gsub(/~/, ENV['HOME']), remote_filepath)
      end while @ssh.loop

      execute_ssh("cat #{remote_filepath} >> #{options[:to]}")
      execute_ssh("rm -f #{remote_filepath}")
    end

    def mkdir_p(path)
      execute_ssh("mkdir -p #{path}")
    end

    private

    def ssh_connect
      initialize_ssh || initialize_ssh_with_password
    end

    #
    # Connect via SSH to the remote server using publickey
    #
    def initialize_ssh
      net_ssh_start(auth_methods: ['publickey'])
    end

    #
    # Connect via SSH to the remote server using password
    #
    def initialize_ssh_with_password
      net_ssh_start(password: @password, number_of_password_prompts: 1)
    end

    #
    # General method to connect via SSH with state machine management
    #
    def net_ssh_start(options)
      before_ssh_start(publickey: options[:auth_methods] == ['publickey'])

      @ssh = Net::SSH.start(@hostname, @username, options)

      after_ssh_start(state: :ok)
    rescue SocketError, Net::SSH::AuthenticationFailed => error
      after_ssh_start(state: :error, message: error.message)
      nil
    end

    def ssh_connected?
      @ssh
    end

    def execute_ssh(command)
      begin
        @ssh.open_channel do |channel|
          channel.request_pty do |_, _|
            channel.exec(command) do |_, _|
              channel.on_data do |_, data|
                @output << data
                channel.send_data "#{@password}\n" if data =~ /password/
              end
            end
          end
        end
      end while @ssh.loop
    end

    def before_ssh_start(options = { publickey: false })
      connect_with = options[:publickey] == true ? 'pubickey' : 'password'
      @state_machine.send("connect_with_#{connect_with}")
    end

    #
    # Callback method executed after the Net::SSH.start
    #
    def after_ssh_start(options = {})
      @output << options[:message] if options[:state] == :error
      ssh_connected? ? @state_machine.connected : @state_machine.not_connected
    end
  end
end
