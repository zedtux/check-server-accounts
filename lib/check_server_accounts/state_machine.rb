module CheckServerAccounts
  #
  # The state machine allows to store complex status
  #
  class StateMachine
    class NotAllowedTransitionError < StandardError; end

    def initialize
      @state = :new
      @ssh_key_missing = false
    end

    def state_is?(state)
      @state == state
    end

    def connect_with_pubickey
      raise_transition_error('connect_with_pubickey') unless state_is?(:new)

      @state = :connecting_with_pubickey
    end

    def connect_with_password
      unless state_is?(:connecting_with_pubickey) ||
             state_is?(:not_connected)
        raise_transition_error('connect_with_password')
      end

      @state = :connecting_with_password
    end

    def connected
      unless state_is?(:connecting_with_pubickey) ||
             state_is?(:connecting_with_password)
        raise_transition_error('connected')
      end

      @state = :connected
    end

    def not_connected
      unless state_is?(:connecting_with_pubickey) ||
             state_is?(:connecting_with_password)
        raise_transition_error_to('not_connected')
      end

      @ssh_key_missing = true if @state == :connecting_with_pubickey

      @state = :not_connected
    end

    private

    def raise_transition_error(to_state)
      raise NotAllowedTransitionError, "Switching from #{@state} to #{to_state}"
    end
  end
end
