module CheckServerAccounts
  #
  # Determine what action to be performed based on the parsed result from SSH
  #
  module DecisionMaker
    def self.decide_from(result, options)
      read_options(options)

      decide_action_to_be_taken_from(result)
    end

    private

    def self.read_options(options)
      @install_ssh_key = options[:install_ssh_key] || false
    end

    def self.decide_action_to_be_taken_from(result)
      case
      when @install_ssh_key && result == ResultParser::NO_SSH_KEY
        :install_ssh_key
      end
    end
  end
end
