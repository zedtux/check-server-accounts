module CheckServerAccounts
  #
  # Parse result and prepare result message to be display in the console
  #
  module ResultParser
    YOUR_PASSWORD_EXPIRED = 'Your password has expired'
    AUTHENTICATION_FAILED = 'Authentication failed for user'
    NOT_REACHABLE = 'nodename nor servname provided, or not known'
    INCORRECT_PASSWORD = 'incorrect password attempts'
    MAY_RUN_SUDO_COMMAND = 'may run the following commands on this host:'
    USER_NOT_ALLOWED_SUDO = 'is not allowed to execute'

    PASSWORD_EXPIRED = 'PASSWORD EXPIRED'
    NO_SSH_KEY = 'NO SSH KEY'
    UNREACHABLE = 'UNREACHABLE'
    WRONG_PASSWORD = 'WRONG PASSWORD'
    OK = 'OK'
    SUDO_NOT_ALLOW = 'SUDO NOT ALLOWED'

    def self.parse(output)
      case
      when output.include?(YOUR_PASSWORD_EXPIRED) then PASSWORD_EXPIRED
      when output.include?(AUTHENTICATION_FAILED) then NO_SSH_KEY
      when output.include?(NOT_REACHABLE)         then UNREACHABLE
      when output.include?(INCORRECT_PASSWORD)    then WRONG_PASSWORD
      when output.include?(MAY_RUN_SUDO_COMMAND)  then OK
      when output.include?(USER_NOT_ALLOWED_SUDO) then SUDO_NOT_ALLOW
      else
        puts "Unknown case: '#{output}'"
      end.dup # Avoid to change the constant content
    end
  end
end
