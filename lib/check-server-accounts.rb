require 'check_server_accounts/action'
require 'check_server_accounts/decision_maker'
require 'check_server_accounts/options_parser'
require 'check_server_accounts/result_parser'
require 'check_server_accounts/ssh_client'
require 'check_server_accounts/state_machine'
require 'check_server_accounts/version'

#
# Main module executing the SSH connection and trying to list the files
#
module CheckServerAccounts
  class SudoImpossibleError < StandardError; end

  def self.start!(options = {})
    @exit_code = 0

    read_options(options)
    extra_message = ' without password' if @password.nil?

    puts "Checking #{@servers.size} servers ..."
    puts

    @servers.each do |hostname|
      STDOUT.write "\rChecking account #{username}#{extra_message} on " \
                   "server #{hostname} ... "

      @state = StateMachine.new

      ssh    = ssh_and_sudo_hostname(hostname)
      result = ResultParser.parse(ssh.output)

      if ssh.connected?
        action = DecisionMaker.decide_from(result, options)
        Action.take(action, ssh)
        result << ' | INSTALLED' if action == :install_ssh_key
      else
        result << ' | CANNOT CONNECT'
      end

      puts result
    end

    exit(@exit_code)
  end

  private

  def self.read_options(options)
    @username = options[:username]
    @password = options[:password]
    @servers = options[:servers]
  end

  def self.username
    @username || ENV['USER']
  end

  def self.ssh_and_sudo_hostname(hostname)
    ssh_client = SshClient.new(hostname, username, @password, @state)
    ssh_client.connect
    ssh_client
  end
end
