# Check Server Accounts

This gem install the `check-servers-account` which connects using SSH to all
your servers and ensure the login is working and also your password is still
valid.

The application expect to connect to the given servers using an SSH key.
In the case it worked, the application is then checking the `sudo` usage in
order to validate that the given user can use it.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'check-server-accounts'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install check-server-accounts

## Usage

```bash
ERROR: You must provide at least one server.

Usage: ./exe/check-server-accounts [options]
    -h, --help                       Show the help message
    -s, --server SERVER_NAME         Server to be checked
    -u, --username USERNAME          Username to be used when connecting to a server
    -p, --password PASSWORD          Password to be used when running sudo command
```

To check some servers with a different username than your current one:

```bash
check-server-accounts -u zedtux -s server1.com -s server2.com -s server3.com -s server4.com
Checking 4 servers ...

Checking account zedtux on server server1.com ... WRONG PASSWORD
Checking account zedtux on server server2.com ... PASSWORD EXPIRED
Checking account zedtux on server server3.com ... NO SSH KEY
Checking account zedtux on server server4.com ... OK
```

#### What sudo command is run

This application is using the `sudo -l -U <username>` command in order to
determine if the login and password are good to run it.
In this case the 'OK' is shown.

## Development

After checking out the repo, run `bin/setup` to install dependencies.
You can also run `bin/console` for an interactive prompt that will allow you to
experiment.

To install this gem onto your local machine, run `bundle exec rake install`.
To release a new version, update the version number in `version.rb`, and then
run `bundle exec rake release`, which will create a git tag for the version,
push git commits and tags, and push the `.gem` file to
[rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at
https://github.com/zedtux/check-server-accounts.
This project is intended to be a safe, welcoming space for collaboration,
and contributors are expected to adhere to the
[Contributor Covenant](CODE_OF_CONDUCT.md) code of conduct.


## License

The gem is available as open source under the terms of the
[MIT License](http://opensource.org/licenses/MIT).
